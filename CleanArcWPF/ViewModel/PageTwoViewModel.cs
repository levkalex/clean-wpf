﻿using CleanArcWPF.Commands;
using CleanArcWPF.Core.CQRS;
using CleanArcWPF.Core.Redirect;
using CleanArcWPF.Queries;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;

namespace CleanArcWPF.ViewModel
{
    public class PageTwoViewModel : IViewModel<Page2>, INotifyPropertyChanged
    {
        public string Text { get; set; }
        public PageTwoViewModel(IRedirect redirect, IDispatcher dispatcher)
        {
            Text = "Text from view model.";

            //var list = dispatcher.Dispatch<IEnumerable<Questions>>(new QuestionFilterQuery() { Count = 3 }).Result;
            //var list2 = dispatcher.Dispatch<int>(new CreateNewQuestionCommand() { Text = "Test" }).Result;
            //var list3 = dispatcher.Dispatch(new CreateNewAnswerCommand() { Text = "Test" });


        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }
    }
}

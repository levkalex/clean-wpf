﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CleanArcWPF.Core.CQRS
{
    public interface IQuery
    {
    }
    
    public class PaginationFilter
    {
        int? Skip { get; }
        int? Take { get; }
    }

  
    public interface IQueryHandler<in TQuery, TResponse> where TQuery : IQuery
    {
        public Task<TResponse> Handle(TQuery query);
    }

   

    //example
    public class Questions
    {

    }
}

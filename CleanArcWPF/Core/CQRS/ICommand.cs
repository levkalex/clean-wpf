﻿using System.Threading.Tasks;

namespace CleanArcWPF.Core.CQRS
{
    public interface ICommand
    {
    }

    public interface ICommandHandler<in TCommand, TResponse> where TCommand : ICommand
    {
        public Task<TResponse> Handle(TCommand command);
    }

    public interface ICommandHandler<in TCommand> where TCommand : ICommand
    {
        public Task Handle(TCommand command);
    }
}

﻿using System.Windows;
using System.Windows.Controls;

namespace CleanArcWPF.Core.Redirect
{
    public interface IRedirect
    {
        void To<T>() where T : UserControl;
        void To<T>(string title) where T : UserControl;
    }
}

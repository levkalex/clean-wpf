﻿using Autofac;
using Autofac.Core;
using CleanArcWPF.ViewModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace CleanArcWPF.Core.Redirect
{
    public class Redirect : IRedirect
    {
        ILifetimeScope _scope;
        MainWindow _window;

        public Redirect(ILifetimeScope scope)
        {
            _scope = scope;
            _window = new MainWindow();
            _window.Show();
        }

        public void To<T>(string title) where T : UserControl
        {
            var page = _scope.Resolve<T>();

            var viewModelType = _scope.ComponentRegistry.Registrations.Single(x => x.Services
                   .OfType<IServiceWithType>()
                   .Any(x => typeof(IViewModel<T>)
                   .IsAssignableFrom(x.ServiceType)))
                    .Activator.LimitType;

            var viewModel = _scope.Resolve(viewModelType);
            page.DataContext = viewModel;

            _window.Title = title;
            _window.Content.Children.Clear();
            _window.Content.Children.Add(page);
        }

        public void To<T>() where T : UserControl
        {
            To<T>(_window.Title);
        }
    }
}

﻿using CleanArcWPF.Core.Redirect;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CleanArcWPF
{
    /// <summary>
    /// Interaction logic for Page2.xaml
    /// </summary>
    public partial class Page2 : UserControl
    {
        IRedirect _redirect;
        public Page2(IRedirect redirect)
        {
            InitializeComponent();
            _redirect = redirect;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            _redirect.To<Page1>();

        }
    }
}

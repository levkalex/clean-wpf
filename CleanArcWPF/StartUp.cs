﻿using Autofac;
using CleanArcWPF.Core.CQRS;
using CleanArcWPF.Core.Extensions;
using CleanArcWPF.Core.Redirect;
using CleanArcWPF.ViewModel;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;

namespace CleanArcWPF
{
    class StartUp
    {
        public static IContainer CompositionRoot()
        {
            var builder = new ContainerBuilder();
            var executingAssembly = Assembly.GetExecutingAssembly();
            var allFormTypes = executingAssembly.GetTypes().Where(x => x.IsClass && x.IsSubclassOf(typeof(UserControl)));
            var allViewModelsTypes = executingAssembly.GetTypesImplementingGenericType(typeof(IViewModel<>));

            builder.RegisterTypes(allFormTypes.ToArray());
            builder.RegisterTypes(allViewModelsTypes.ToArray());

            var allQueryHandlerTypes = executingAssembly.GetTypesImplementingGenericType(typeof(IQueryHandler<,>));
            var allCommandHandlerTypes = executingAssembly.GetTypesImplementingGenericType(typeof(ICommandHandler<,>));
            var allCommandHandlerTypesWithoutReturnType = executingAssembly.GetTypesImplementingGenericType(typeof(ICommandHandler<>));

            builder.RegisterTypes(allQueryHandlerTypes.ToArray());
            builder.RegisterTypes(allCommandHandlerTypes.ToArray());
            builder.RegisterTypes(allCommandHandlerTypesWithoutReturnType.ToArray());

            builder.RegisterType<Redirect>().As<IRedirect>().SingleInstance();
            builder.RegisterType<Dispatcher>().As<IDispatcher>();
            return builder.Build();
        }

        public StartUp()
        {
            var container = CompositionRoot();
            var iRedirect = container.Resolve<IRedirect>();

            iRedirect.To<Page1>();
        }
    }
}

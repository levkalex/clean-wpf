﻿using CleanArcWPF.Core.CQRS;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CleanArcWPF.Queries
{
    public class QuestionFilterQuery : PaginationFilter, IQuery
    {
        public int Count { get; set; }
    }

    public class FetchQuestionQueryHandler : IQueryHandler<QuestionFilterQuery, IEnumerable<Questions>>
    {
        public Task<IEnumerable<Questions>> Handle(QuestionFilterQuery query)
        {
            //Fetch from db
            var list = Enumerable.Range(0, query.Count).Select(x=> new Questions { });

            return Task.FromResult(list);
        }
    }

}

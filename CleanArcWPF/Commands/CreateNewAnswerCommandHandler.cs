﻿using CleanArcWPF.Core.CQRS;
using System.Threading.Tasks;

namespace CleanArcWPF.Commands
{
    public class CreateNewAnswerCommand : ICommand
    {
        public string Text { get; set; }
    }

    public class CreateNewAnswerCommandHandler : ICommandHandler<CreateNewAnswerCommand>
    {
        public Task Handle(CreateNewAnswerCommand command)
        {
            //Save in database
            //command.Answer, command.Text
            //return Task.CompletedTask
            return Task.CompletedTask;
        }
    }
}

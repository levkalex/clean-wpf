﻿using CleanArcWPF.Core.CQRS;
using System.Threading.Tasks;

namespace CleanArcWPF.Commands
{
    public class CreateNewQuestionCommand : ICommand
    {
        public string Text { get; set; }
        public string Answer { get; set; }
    }

    public class CreateNewQuestionCommandHandler : ICommandHandler<CreateNewQuestionCommand, int>
    {
        public Task<int> Handle(CreateNewQuestionCommand command)
        {
            //Save in database
            //command.Answer, command.Text
            //return id from db
            return Task.FromResult(99);
        }
    }
}
